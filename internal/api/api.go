package api

import (
	"encoding/csv"
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/box"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

var (
	ErrValidationArgs = errors.New("Bad argument")
    ErrValidationGtins = errors.New("Wrong gtins")
)

var (
	aggrCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "aggregations",
		Help: "Количество операций аггрегации",
	})
    aggrDuration = promauto.NewHistogram(prometheus.HistogramOpts{
        Name: "aggregatins_duration",
        Help: "Продолжительность аггрегаций",
    })
)

type ApiServer struct {
    boxApp boxapp.App
    packApp packapp.App
    productApp productapp.App
}

func New(boxApp boxapp.App, packApp packapp.App, productApp productapp.App) ApiServer {
    return ApiServer{
        boxApp: boxApp,
        packApp: packApp,
        productApp: productApp,
    }
}

func (a *ApiServer) Aggregation(rw http.ResponseWriter, req *http.Request) {
    aggrCounter.Inc()
    timer := prometheus.NewTimer(aggrDuration)
    defer timer.ObserveDuration()


    rw.Header().Set("Content-Type", "application/json")
    encoder := json.NewEncoder(rw)

	decoder := json.NewDecoder(req.Body)
    decoder.DisallowUnknownFields()
    var m = struct {
        Sscc string // 14\d
        Created string
        Sgtins []string // 27 - 14\d, 13\da-zA-Z
    }{}
    err := decoder.Decode(&m)
    if err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "Неправильный формат запроса", ErrorCode: 2})
        return
    }
    log.Infof("Get msg: %s", m)

    if err = a.checkGtins(m.Sgtins); err != nil {
        log.Error(err)
        if errors.Is(err, ErrValidationGtins) {
            encoder.Encode(struct {
                Ok bool        `json:"ok"`
                Error string   `json:"error"`
                ErrorCode int  `json:"error_code"`
            }{Ok: false, Error: "Неправильные GTINS", ErrorCode: 3})
            return
        }
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "internal error", ErrorCode: 13})
        return
    }
    
    packs := make([]pack.Pack, len(m.Sgtins))
    for i, sgtin := range m.Sgtins {
        packs[i] = pack.NewPack(sgtin, m.Sscc, getGtin(sgtin))
    }

    b := box.NewBox(m.Sscc, m.Created, packs)
    if err = a.boxApp.Add(b); err != nil {
        if errors.Is(err, boxapp.ErrBoxExists) {
            log.Error(err)
            encoder.Encode(struct {
                Ok bool        `json:"ok"`
                Error string   `json:"error"`
                ErrorCode int  `json:"error_code"`
            }{Ok: false, Error: "SSCC уже использован", ErrorCode: 1})
            return
        } else if errors.Is(err, packapp.ErrPackExists) {
            log.Error(err)
            encoder.Encode(struct {
                Ok bool        `json:"ok"`
                Error string   `json:"error"`
                ErrorCode int  `json:"error_code"`
            }{Ok: false, Error: "SGTIN уже использован", ErrorCode: 1})
            return
        }

        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "internal error", ErrorCode: 13})
        return
    }
    
    encoder.Encode(struct {
        Ok bool `json:"ok"`
    }{Ok: true})
}

func (a *ApiServer) GetBoxesBySgtins(rw http.ResponseWriter, req *http.Request) {
    rw.Header().Set("Content-Type", "application/json")
    encoder := json.NewEncoder(rw)

	decoder := json.NewDecoder(req.Body)
    decoder.DisallowUnknownFields()
    var sgtins []string //27 - 14\d, 13\da-zA-Z
    err := decoder.Decode(&sgtins)
    if err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "Неправильный формат запроса", ErrorCode: 2})
        return
    }
    log.Infof("Get msg: %v", sgtins)

    resMap, err := a.packApp.GetBySgtins(sgtins)

    if err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "internal error", ErrorCode: 13})
        return
    }
    log.Infof("Send msg: %v", resMap)
    encoder.Encode(resMap)
}

func (a *ApiServer) GetPacksByGtin(rw http.ResponseWriter, req *http.Request) {
    rw.Header().Set("Content-Type", "application/json")
    encoder := json.NewEncoder(rw)
    writer := csv.NewWriter(rw)

	decoder := json.NewDecoder(req.Body)
    decoder.DisallowUnknownFields()
    var m = struct {
        Gtin string // 14\d
    }{}
    err := decoder.Decode(&m)
    if err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "Неправильный формат запроса", ErrorCode: 2})
        return
    }
    log.Infof("Get msg: %v", m)

    packs, err := a.packApp.GetByGtin(m.Gtin)

    if err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "internal error", ErrorCode: 13})
        return
    }

    records := make([][]string, len(packs))

    for i, pack := range packs {
        records[i] = []string{pack.Sgtin, pack.Sscc}		
	}
    if err := writer.WriteAll(records); err != nil {
        log.Error(err)
        encoder.Encode(struct {
            Ok bool        `json:"ok"`
            Error string   `json:"error"`
            ErrorCode int  `json:"error_code"`
        }{Ok: false, Error: "internal error", ErrorCode: 13})
        return
    }

    log.Infof("Send msg: %v", records)
}

func (a *ApiServer) checkGtins(sgtins []string) error {
    gtin := sgtins[0][:14]
    p, err := a.productApp.GetByGtin(gtin)
    if err != nil {
        if errors.Is(err, productapp.ErrProductNotExists) {
            return ErrValidationGtins
        }
        return err
    }
    if len(sgtins) != p.Packs {
        return ErrValidationGtins
    }

    for _, sgtin := range sgtins[1:] {
        if getGtin(sgtin) != gtin {
            return ErrValidationGtins
        }
    }
    return nil
}

func getGtin(sgtin string) string {
    return sgtin[:14]
}
