package box

import (
	"fmt"

	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

type Box struct {
	Sscc    string   `db:"sscc" `
	Created string   `db:"created"`
	Packs []pack.Pack
}

func NewBox(sscc, created string, packs []pack.Pack) Box {
	return Box{
		Sscc:    sscc,
		Created: created,
		Packs: packs,
	}
}

func (b Box) String() string {
	return fmt.Sprintf("sscc: %s, created: %s, packs: %s", b.Sscc, b.Created, b.Packs)
}

func (b *Box) SetSscc(sscc string) {
	b.Sscc = sscc
}

func (b *Box) SetCreated(created string) {
	b.Created = created
}

func (b *Box) SetPacks(packs []pack.Pack) {
	b.Packs = packs
}

func (b Box) GetSscc() string {
	return b.Sscc
}

func (b Box) GetCreated() string {
	return b.Created
}

func (b Box) GetPacks() []pack.Pack {
	return b.Packs
}
