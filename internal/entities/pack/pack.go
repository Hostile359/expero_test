package pack

import "fmt"

type Pack struct {
	Sgtin string `db:"sgtin"`
	Sscc string  `db:"sscc"`
	Gtin string	 `db:"gtin"`
	
}

func NewPack(sgtin, sscc, gtin string) Pack {
	return Pack{
		Sgtin: sgtin,
		Sscc: sscc,
		Gtin: gtin,
	}
}

func (p Pack) String() string {
	return fmt.Sprintf("sgtin: %s, sscc: %s, gtin: %s", p.Sgtin, p.Sscc, p.Gtin)
}

func (p *Pack) SetSgtin(sgtin string) {
	p.Sgtin = sgtin
}

func (p *Pack) SetSscc(sscc string) {
	p.Sscc = sscc
}

func (p *Pack) SetGtin(gtin string) {
	p.Gtin = gtin
}

func (p *Pack) GetSgtin() string {
	return p.Sgtin
}

func (p *Pack) GetSscc() string {
	return p.Sscc
}

func (p *Pack) GetGtin() string {
	return p.Gtin
}
