package product

import (
	"fmt"
)

type Product struct {
	Name    string   `db:"name" `
	Gtin    string   `db:"gtin"`
	Packs   int      `db:"packs"`
}

func NewProduct(name, gtin string, packs int) Product {
	return Product{
		Name: name,
		Gtin: gtin,
		Packs: packs,
	}
}

func (p Product) String() string {
	return fmt.Sprintf("name: %s, gtin: %s, packs: %d", p.Name, p.Gtin, p.Packs)
}

func (p *Product) SetName(name string) {
	p.Name = name
}

func (p *Product) SetGtin(gtin string) {
	p.Gtin = gtin
}

func (p *Product) SetPacks(packs int) {
	p.Packs = packs
}

func (p Product) GetName() string {
	return p.Name
}

func (p Product) GetGtin() string {
	return p.Gtin
}

func (p Product) GetPacks() int {
	return p.Packs
}
