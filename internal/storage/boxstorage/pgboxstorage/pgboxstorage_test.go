package pgboxstorage

import (
	"context"
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/box"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

func TestAddBox(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		packs := []pack.Pack{
			pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001"),
		}
		b := box.NewBox("000000000000000001", "2022-12-01T06:45:15+07:00", packs)

		queryStore := "INSERT INTO boxes (sscc, created) VALUES ($1,$2)"
		args := []interface{}{b.GetSscc(), b.GetCreated()}
		f.mockPool.EXPECT().Exec(context.Background(), queryStore, args...).Return(nil, nil)

		queryStore1 := "INSERT INTO packs (sgtin, sscc, gtin) VALUES ($1,$2,$3)"
		args1 := []interface{}{packs[0].GetSgtin(), packs[0].GetSscc(), packs[0].GetGtin()}
		f.mockPool.EXPECT().Exec(context.Background(), queryStore1, args1...).Return(nil, nil)

		// act
		err := f.boxStorage.Add(b)

		// assert
		require.NoError(t, err)
	})
}

func TestGetBox(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		b := box.NewBox("000000000000000001", "2022-12-01T06:45:15+07:00", nil)
		queryStore := "SELECT sscc, created FROM boxes WHERE sscc = $1"
		args := []interface{}{b.GetSscc()}
		columns := []string{"sscc", "created"}
		pgxRows := pgxpoolmock.NewRows(columns).AddRow(b.GetSscc(), b.GetCreated()).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)
		
		// act
		actualBox, err := f.boxStorage.Get(b.GetSscc())
		

		// assert
		require.NoError(t, err)
		assert.Equal(t, &b, actualBox)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		b := box.NewBox("000000000000000001", "2022-12-01T06:45:15+07:00", nil)
		queryStore := "SELECT sscc, created FROM boxes WHERE sscc = $1"
		args := []interface{}{b.GetSscc()}
		columns := []string{"sscc", "created"}
		pgxRows := pgxpoolmock.NewRows(columns).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)
		
		// act
		actualBox, err := f.boxStorage.Get(b.GetSscc())

		// assert
		assert.ErrorIs(t, err, boxapp.ErrBoxNotExists)
		assert.Equal(t, (*box.Box)(nil), actualBox)
	})
}
