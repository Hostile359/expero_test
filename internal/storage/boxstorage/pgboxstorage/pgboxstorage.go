//go:generate mockgen -source ../../../app/boxapp/boxapp.go -destination=./mocks/pgboxstorage.go -package=mock_pgboxstorage
package pgboxstorage

import (
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/driftprogramming/pgxpoolmock"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/pkg/errors"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/box"
)

var _ boxapp.Storage = &Storage{}

type Storage struct{
	pool pgxpoolmock.PgxPool
}

func New(pool pgxpoolmock.PgxPool) boxapp.Storage {
	return &Storage{
		pool: pool,
	}
}

// переделать под транзакцию
func (s *Storage) Add(b box.Box) error {
	query, args, err := squirrel.Insert("boxes").
		Columns("sscc, created").
		Values(b.GetSscc(), b.GetCreated()).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return errors.Errorf("Storage.Add: to sql: %v", err)
	}
	if _, err = s.pool.Exec(context.Background(), query, args...); err != nil {
		return errors.Errorf("Storage.Add: insert: %v", err)
	}

	for _, p := range b.Packs {
		query, args, err := squirrel.Insert("packs").
			Columns("sgtin, sscc, gtin").
			Values(p.GetSgtin(), p.GetSscc(), p.GetGtin()).
			PlaceholderFormat(squirrel.Dollar).ToSql()
		if err != nil {
			return errors.Errorf("Storage.Add: to sql: %v", err)
		}
		if _, err = s.pool.Exec(context.Background(), query, args...); err != nil {
			return errors.Errorf("Storage.Add: insert: %v", err)
		}
	}

	return nil
}

func (s *Storage) Get(sscc string) (*box.Box, error) {
	query, args, err := squirrel.Select("sscc, created").
		From("boxes").
		Where(squirrel.Eq{
			"sscc": sscc,
		}).PlaceholderFormat(squirrel.Dollar).ToSql()
	
	if err != nil {
		return nil, errors.Errorf("Storage.Get: to sql: %v", err)
	}
	var boxes []box.Box
	if err := pgxscan.Select(context.Background(), s.pool, &boxes, query, args...); err != nil {
		return nil, errors.Errorf("Storage.Get: select: %v", err)
	}
	
	if len(boxes) == 0 {
		return nil, errors.Wrapf(boxapp.ErrBoxNotExists, "box-sscc: [%s]", sscc)
	}
	return &boxes[0], nil
}
