package pgboxstorage

import (
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/golang/mock/gomock"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
)

type boxStorageFixture struct {
	boxStorage boxapp.Storage
	ctrl *gomock.Controller
	mockPool *pgxpoolmock.MockPgxPool
}

func setUp(t *testing.T) boxStorageFixture {
	var fixture boxStorageFixture
	fixture.ctrl = gomock.NewController(t)
	fixture.mockPool = pgxpoolmock.NewMockPgxPool(fixture.ctrl)
	fixture.boxStorage = New(fixture.mockPool)

	return fixture
}

func (f *boxStorageFixture) tearDown() {
	f.ctrl.Finish()
}