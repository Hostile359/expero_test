//go:generate mockgen -source ../../../app/packapp/packapp.go -destination=./mocks/pgpackstorage.go -package=mock_pgpackstorage
package pgpackstorage

import (
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/driftprogramming/pgxpoolmock"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/pkg/errors"

	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

var _ packapp.Storage = &Storage{}

type Storage struct{
	pool pgxpoolmock.PgxPool
}

func New(pool pgxpoolmock.PgxPool) packapp.Storage {
	return &Storage{
		pool: pool,
	}
}

func (s *Storage) Add(p pack.Pack) error {
	query, args, err := squirrel.Insert("packs").
		Columns("sgtin, sscc, gtin").
		Values(p.GetSgtin(), p.GetSscc(), p.GetGtin()).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return errors.Errorf("Storage.Add: to sql: %v", err)
	}
	if _, err = s.pool.Exec(context.Background(), query, args...); err != nil {
		return errors.Errorf("Storage.Add: insert: %v", err)
	}

	return nil
}

func (s *Storage) Get(sgtin string) (*pack.Pack, error) {
	query, args, err := squirrel.Select("sgtin, sscc, gtin").
		From("packs").
		Where(squirrel.Eq{
			"sgtin": sgtin,
		}).PlaceholderFormat(squirrel.Dollar).ToSql()
	
	if err != nil {
		return nil, errors.Errorf("Storage.Get: to sql: %v", err)
	}
	var packs []pack.Pack
	if err := pgxscan.Select(context.Background(), s.pool, &packs, query, args...); err != nil {
		return nil, errors.Errorf("Storage.Get: select: %v", err)
	}
	if len(packs) == 0 {
		return nil, errors.Wrapf(packapp.ErrPackNotExists, "pack-sgtin: [%s]", sgtin)
	}
	return &packs[0], nil
}

func (s *Storage) GetByGtin(gtin string) ([]pack.Pack, error) {
	query, args, err := squirrel.Select("sgtin, sscc, gtin").
		From("packs").
		Where(squirrel.Eq{
			"gtin": gtin,
		}).PlaceholderFormat(squirrel.Dollar).ToSql()
	
	if err != nil {
		return nil, errors.Errorf("Storage.Get: to sql: %v", err)
	}
	var packs []pack.Pack
	if err := pgxscan.Select(context.Background(), s.pool, &packs, query, args...); err != nil {
		return nil, errors.Errorf("Storage.Get: select: %v", err)
	}
	
	return packs, nil
}
