package pgpackstorage

import (
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/golang/mock/gomock"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
)

type packStorageFixture struct {
	packStorage packapp.Storage
	ctrl *gomock.Controller
	mockPool *pgxpoolmock.MockPgxPool
}

func setUp(t *testing.T) packStorageFixture {
	var fixture packStorageFixture
	fixture.ctrl = gomock.NewController(t)
	fixture.mockPool = pgxpoolmock.NewMockPgxPool(fixture.ctrl)
	fixture.packStorage = New(fixture.mockPool)

	return fixture
}

func (f *packStorageFixture) tearDown() {
	f.ctrl.Finish()
}