package pgpackstorage

import (
	"context"
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

func TestAddPack(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")
		queryStore := "INSERT INTO packs (sgtin, sscc, gtin) VALUES ($1,$2,$3)"
		args := []interface{}{p.GetSgtin(), p.GetSscc(), p.GetGtin()}
		f.mockPool.EXPECT().Exec(context.Background(), queryStore, args...).Return(nil, nil)

		// act
		err := f.packStorage.Add(p)

		// assert
		require.NoError(t, err)
	})
}

func TestGetPack(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")
		queryStore := "SELECT sgtin, sscc, gtin FROM packs WHERE sgtin = $1"
		args := []interface{}{p.GetSgtin()}
		columns := []string{"sgtin", "sscc", "gtin"}
		pgxRows := pgxpoolmock.NewRows(columns).AddRow(p.GetSgtin(), p.GetSscc(), p.GetGtin()).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)
		
		// act
		actualPack, err := f.packStorage.Get(p.GetSgtin())
		

		// assert
		require.NoError(t, err)
		assert.Equal(t, &p, actualPack)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")
		queryStore := "SELECT sgtin, sscc, gtin FROM packs WHERE sgtin = $1"
		args := []interface{}{p.GetSgtin()}
		columns := []string{"sgtin", "sscc", "gtin"}
		pgxRows := pgxpoolmock.NewRows(columns).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)
		
		// act
		actualPack, err := f.packStorage.Get(p.GetSgtin())

		// assert
		assert.ErrorIs(t, err, packapp.ErrPackNotExists)
		assert.Equal(t, (*pack.Pack)(nil), actualPack)
	})
}

func TestGetByGtin(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		packs := []pack.Pack{pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")}
		queryStore := "SELECT sgtin, sscc, gtin FROM packs WHERE gtin = $1"
		args := []interface{}{packs[0].GetGtin()}
		columns := []string{"sgtin", "sscc", "gtin"}
		pgxRows := pgxpoolmock.NewRows(columns).AddRow(packs[0].GetSgtin(), packs[0].GetSscc(), packs[0].GetGtin()).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)
		
		// act
		actualPacks, err := f.packStorage.GetByGtin(packs[0].GetGtin())
		

		// assert
		require.NoError(t, err)
		assert.Equal(t, packs, actualPacks)
	})
}
