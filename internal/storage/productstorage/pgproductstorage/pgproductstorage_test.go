package pgproductstorage

import (
	"context"
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/product"
)

func TestAddProduct(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		queryStore := "INSERT INTO products (name, gtin, packs) VALUES ($1,$2,$3)"
		args := []interface{}{p.GetName(), p.GetGtin(), p.GetPacks()}
		f.mockPool.EXPECT().Exec(context.Background(), queryStore, args...).Return(nil, nil)

		// act
		err := f.productStorage.Add(p)

		// assert
		require.NoError(t, err)
	})
}

func TestGetByGtin(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		queryStore := "SELECT name, gtin, packs FROM products WHERE gtin = $1"
		args := []interface{}{p.GetGtin()}
		columns := []string{"name", "gtin", "packs"}
		pgxRows := pgxpoolmock.NewRows(columns).AddRow(p.GetName(), p.GetGtin(), p.GetPacks()).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)

		// act
		actualProduct, err := f.productStorage.GetByGtin(p.GetGtin())

		// assert
		require.NoError(t, err)
		assert.Equal(t, &p, actualProduct)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P2", "04603988007777", 12)
		queryStore := "SELECT name, gtin, packs FROM products WHERE gtin = $1"
		args := []interface{}{p.GetGtin()}
		columns := []string{"name", "gtin", "packs"}
		pgxRows := pgxpoolmock.NewRows(columns).ToPgxRows()
		f.mockPool.EXPECT().Query(context.Background(), queryStore, args...).Return(pgxRows, nil)

		// act
		actualProduct, err := f.productStorage.GetByGtin(p.GetGtin())

		// assert
		assert.ErrorIs(t, err, productapp.ErrProductNotExists)
		assert.Equal(t, (*product.Product)(nil), actualProduct)
	})
}
