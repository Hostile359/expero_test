package pgproductstorage

import (
	"testing"

	"github.com/driftprogramming/pgxpoolmock"
	"github.com/golang/mock/gomock"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
)

type productStorageFixture struct {
	productStorage productapp.Storage
	ctrl *gomock.Controller
	mockPool *pgxpoolmock.MockPgxPool
}

func setUp(t *testing.T) productStorageFixture {
	var fixture productStorageFixture
	fixture.ctrl = gomock.NewController(t)
	fixture.mockPool = pgxpoolmock.NewMockPgxPool(fixture.ctrl)
	fixture.productStorage = New(fixture.mockPool)

	return fixture
}

func (f *productStorageFixture) tearDown() {
	f.ctrl.Finish()
}
