//go:generate mockgen -source ../../../app/productapp/productapp.go -destination=./mocks/pgproductstorage.go -package=mock_pgproductstorage
package pgproductstorage

import (
	"context"

	"github.com/Masterminds/squirrel"
	"github.com/driftprogramming/pgxpoolmock"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/pkg/errors"

	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/product"
)

var _ productapp.Storage = &Storage{}

type Storage struct{
	pool pgxpoolmock.PgxPool
}

func New(pool pgxpoolmock.PgxPool) productapp.Storage {
	return &Storage{
		pool: pool,
	}
}

func (s *Storage) Add(p product.Product) error {
	query, args, err := squirrel.Insert("products").
		Columns("name, gtin, packs").
		Values(p.GetName(), p.GetGtin(), p.GetPacks()).
		PlaceholderFormat(squirrel.Dollar).ToSql()
	if err != nil {
		return errors.Errorf("Storage.Add: to sql: %v", err)
	}
	if _, err = s.pool.Exec(context.Background(), query, args...); err != nil {
		return errors.Errorf("Storage.Add: insert: %v", err)
	}

	return nil
}

func (s *Storage) GetByGtin(gtin string) (*product.Product, error) {
	query, args, err := squirrel.Select("name, gtin, packs").
		From("products").
		Where(squirrel.Eq{
			"gtin": gtin,
		}).PlaceholderFormat(squirrel.Dollar).ToSql()
	
	if err != nil {
		return nil, errors.Errorf("Storage.Get: to sql: %v", err)
	}
	var products []product.Product
	if err := pgxscan.Select(context.Background(), s.pool, &products, query, args...); err != nil {
		return nil, errors.Errorf("Storage.Get: select: %v", err)
	}
	
	if len(products) == 0 {
		return nil, errors.Wrapf(productapp.ErrProductNotExists, "product-gtin: [%s]", gtin)
	}
	return &products[0], nil
}