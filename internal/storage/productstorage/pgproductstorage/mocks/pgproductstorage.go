// Code generated by MockGen. DO NOT EDIT.
// Source: ../../../app/productapp/productapp.go

// Package mock_pgproductstorage is a generated GoMock package.
package mock_pgproductstorage

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
	product "gitlab.com/Hostile359/expero_test/internal/entities/product"
)

// MockStorage is a mock of Storage interface.
type MockStorage struct {
	ctrl     *gomock.Controller
	recorder *MockStorageMockRecorder
}

// MockStorageMockRecorder is the mock recorder for MockStorage.
type MockStorageMockRecorder struct {
	mock *MockStorage
}

// NewMockStorage creates a new mock instance.
func NewMockStorage(ctrl *gomock.Controller) *MockStorage {
	mock := &MockStorage{ctrl: ctrl}
	mock.recorder = &MockStorageMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockStorage) EXPECT() *MockStorageMockRecorder {
	return m.recorder
}

// Add mocks base method.
func (m *MockStorage) Add(arg0 product.Product) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Add", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// Add indicates an expected call of Add.
func (mr *MockStorageMockRecorder) Add(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Add", reflect.TypeOf((*MockStorage)(nil).Add), arg0)
}

// GetByGtin mocks base method.
func (m *MockStorage) GetByGtin(arg0 string) (*product.Product, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetByGtin", arg0)
	ret0, _ := ret[0].(*product.Product)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetByGtin indicates an expected call of GetByGtin.
func (mr *MockStorageMockRecorder) GetByGtin(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetByGtin", reflect.TypeOf((*MockStorage)(nil).GetByGtin), arg0)
}
