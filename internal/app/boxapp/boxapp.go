package boxapp

import (
	"github.com/pkg/errors"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/box"
)

var (
	ErrBoxNotExists = errors.New("box does not exist")
	ErrBoxExists = errors.New("box exists")
)

type Storage interface {
	Add(box.Box) error
	Get(string) (*box.Box, error)
}

type App struct {
	boxStorage Storage
	packApp packapp.App
}

func New(boxStorage Storage, packApp packapp.App) *App {
	return &App{
		boxStorage: boxStorage,
		packApp: packApp,
	}
}

func (a *App) Add(b box.Box) error {
	_, err := a.boxStorage.Get(b.Sscc)
	if err != nil && !errors.Is(err, ErrBoxNotExists){
		return err
	} else if err == nil {
		return ErrBoxExists
	}

	for _, pack := range b.GetPacks() {
		_, err := a.packApp.Get(pack.GetSgtin())
		if err != nil && !errors.Is(err, packapp.ErrPackNotExists){
			return err
		} else if err == nil {
			return packapp.ErrPackExists
		}
	}

	return a.boxStorage.Add(b)
}
