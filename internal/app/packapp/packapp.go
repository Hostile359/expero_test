package packapp

import (
	"github.com/pkg/errors"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

var (
	ErrPackNotExists = errors.New("pack does not exist")
	ErrPackExists = errors.New("pack exists")
)

type Storage interface {
	Add(pack.Pack) error
	Get(string) (*pack.Pack, error)
	GetByGtin(string) ([]pack.Pack, error)
}

type App struct {
	packStorage Storage
}

func New(packStorage Storage) *App {
	return &App{
		packStorage: packStorage,
	}
}

func (a *App) Get(sgtin string) (*pack.Pack, error) {
	return a.packStorage.Get(sgtin)
}

func (a *App) GetBySgtins(sgtins []string) (map[string]interface{}, error) {
	resMap := make(map[string]interface{})

	for _, sgtin := range sgtins {
		pack, err := a.packStorage.Get(sgtin)
		if err != nil {
			if !errors.Is(err, ErrPackNotExists) {
				return nil, err
			}
			resMap[sgtin] = nil
		}else {
			resMap[sgtin] = pack.GetSscc()
		}
	}

	return resMap, nil
}

func (a *App) GetByGtin(gtin string) ([]pack.Pack, error) {
	return a.packStorage.GetByGtin(gtin)
}
