package packapp

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Hostile359/expero_test/internal/entities/pack"
)

func TestGetPack(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")
		f.mockStorage.EXPECT().Get(p.GetSgtin()).Return(&p, nil)

		// act
		actualPack, err := f.packApp.Get(p.GetSgtin())

		// assert
		require.NoError(t, err)
		assert.Equal(t, &p, actualPack)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")
		f.mockStorage.EXPECT().Get(p.GetSgtin()).Return(nil, ErrPackNotExists)

		// act
		actualPack, err := f.packApp.Get(p.GetSgtin())

		// assert
		assert.ErrorIs(t, err, ErrPackNotExists)
		assert.Equal(t, (*pack.Pack)(nil), actualPack)
	})
}

func TestGetByStins(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()
		
		sgtins := []string{"04603988000001IE9HALA4IBIH1", "04603988000001IE9HALA4IBIH2", "04603988000001IE9HALA4IBIH3"}
		packs := []pack.Pack{
			pack.NewPack(sgtins[0], "000000000000000001", "04603988000001"),
			pack.NewPack(sgtins[1], "000000000000000001", "04603988000001"),
		}
		expectedMap := map[string]interface{}{
			sgtins[0]: packs[0].GetSscc(),
			sgtins[1]: packs[1].GetSscc(),
			sgtins[2]: nil,
		}
		f.mockStorage.EXPECT().Get(sgtins[0]).Return(&packs[0], nil)
		f.mockStorage.EXPECT().Get(sgtins[1]).Return(&packs[1], nil)
		f.mockStorage.EXPECT().Get(sgtins[2]).Return(nil, ErrPackNotExists)
		
		// act
		actualMap, err := f.packApp.GetBySgtins(sgtins)
		

		// assert
		require.NoError(t, err)
		assert.Equal(t, expectedMap, actualMap)
	})
}

func TestGetByGtin(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		packs := []pack.Pack{pack.NewPack("04603988000001IE9HALA4IBIH1", "000000000000000001", "04603988000001")}
		f.mockStorage.EXPECT().GetByGtin(packs[0].GetGtin()).Return(packs, nil)
		
		// act
		actualPacks, err := f.packApp.GetByGtin(packs[0].GetGtin())
		

		// assert
		require.NoError(t, err)
		assert.Equal(t, packs, actualPacks)
	})
}
