package packapp

import (
	"testing"

	"github.com/golang/mock/gomock"
	mock_pgpackstorage "gitlab.com/Hostile359/expero_test/internal/storage/packstorage/pgpackstorage/mocks"

)

type packAppFixture struct {
	packApp *App
	ctrl *gomock.Controller
	mockStorage *mock_pgpackstorage.MockStorage
}

func setUp(t *testing.T) packAppFixture {
	var fixture packAppFixture
	fixture.ctrl = gomock.NewController(t)
	fixture.mockStorage = mock_pgpackstorage.NewMockStorage(fixture.ctrl)
	fixture.packApp = New(fixture.mockStorage)

	return fixture
}

func (f *packAppFixture) tearDown() {
	f.ctrl.Finish()
}