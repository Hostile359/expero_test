package productapp

import (
	"testing"

	"github.com/golang/mock/gomock"
	mock_pgproductstorage "gitlab.com/Hostile359/expero_test/internal/storage/productstorage/pgproductstorage/mocks"

)

type productAppFixture struct {
	productApp *App
	ctrl *gomock.Controller
	mockStorage *mock_pgproductstorage.MockStorage
}

func setUp(t *testing.T) productAppFixture {
	var fixture productAppFixture
	fixture.ctrl = gomock.NewController(t)
	fixture.mockStorage = mock_pgproductstorage.NewMockStorage(fixture.ctrl)
	fixture.productApp = New(fixture.mockStorage)

	return fixture
}

func (f *productAppFixture) tearDown() {
	f.ctrl.Finish()
}