package productapp

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/Hostile359/expero_test/internal/entities/product"
)

func TestAddProduct(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		f.mockStorage.EXPECT().GetByGtin(p.GetGtin()).Return(nil, ErrProductNotExists)
		f.mockStorage.EXPECT().Add(p).Return(nil)

		// act
		err := f.productApp.Add(p)

		// assert
		require.NoError(t, err)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		f.mockStorage.EXPECT().GetByGtin(p.GetGtin()).Return(&p, nil)

		// act
		err := f.productApp.Add(p)

		// assert
		assert.ErrorIs(t, err, ErrProductExists)
	})
}

func TestGetByGtin(t *testing.T) {
	t.Run("succes", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		f.mockStorage.EXPECT().GetByGtin(p.GetGtin()).Return(&p, nil)

		// act
		actualProduct, err := f.productApp.GetByGtin(p.GetGtin())

		// assert
		require.NoError(t, err)
		assert.Equal(t, &p, actualProduct)
	})

	t.Run("fail", func(t *testing.T) {
		// arrange
		f := setUp(t)
		defer f.tearDown()

		p := product.NewProduct("P1", "04603988000001", 4)
		f.mockStorage.EXPECT().GetByGtin(p.GetGtin()).Return(nil, ErrProductNotExists)

		// act
		actualProduct, err := f.productApp.GetByGtin(p.GetGtin())

		// assert
		assert.ErrorIs(t, err, ErrProductNotExists)
		assert.Equal(t, (*product.Product)(nil), actualProduct)
	})
}
