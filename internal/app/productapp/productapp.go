package productapp

import (
	"github.com/pkg/errors"
	"gitlab.com/Hostile359/expero_test/internal/entities/product"
)

var (
	ErrProductNotExists = errors.New("product does not exist")
	ErrProductExists = errors.New("product exists")
)

type Storage interface {
	Add(product.Product) error
	GetByGtin(string) (*product.Product, error)
}

type App struct {
	productStorage Storage
}

func New(productStorage Storage) *App {
	return &App{
		productStorage: productStorage,
	}
}

func (a *App) Add(p product.Product) error {
	_, err := a.GetByGtin(p.Gtin)
	if err != nil && !errors.Is(err, ErrProductNotExists) {
		return err
	} else if err == nil {
		return ErrProductExists
	}

	return a.productStorage.Add(p)
}

func (a *App) GetByGtin(gtin string) (*product.Product, error) {
	return a.productStorage.GetByGtin(gtin)
}
