-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS public.boxes (
    sscc      text         NOT NULL CONSTRAINT sscc_right CHECK(length(sscc) = 18) PRIMARY KEY,
    created   text         NOT NULL  
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS public.boxes CASCADE;
-- +goose StatementEnd
