-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS public.packs (
    sgtin     text     NOT NULL CONSTRAINT sgtin_right CHECK(length(sgtin) = 27) PRIMARY KEY,
    gtin      text     NOT NULL CONSTRAINT gtin_right CHECK(length(gtin) = 14),
    sscc      text     NOT NULL,
    CONSTRAINT fk_boxex
        FOREIGN KEY(sscc)
        REFERENCES boxes(sscc)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS public.packs CASCADE;
-- +goose StatementEnd
