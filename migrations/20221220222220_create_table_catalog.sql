-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS public.products (
    name      text         NOT NULL,
    gtin      text         NOT NULL CONSTRAINT gtin_right CHECK(length(gtin) = 14) PRIMARY KEY,
    packs     integer      NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS public.products;
-- +goose StatementEnd
