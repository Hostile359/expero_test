package main

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	log "github.com/sirupsen/logrus"
	initproducts "gitlab.com/Hostile359/expero_test/cmd/init_products"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
	"gitlab.com/Hostile359/expero_test/internal/storage/boxstorage/pgboxstorage"
	"gitlab.com/Hostile359/expero_test/internal/storage/packstorage/pgpackstorage"
	"gitlab.com/Hostile359/expero_test/internal/storage/productstorage/pgproductstorage"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	
	cfg, err := NewConfig()
	if err != nil {
		log.Fatal("Error while loading config: ", err)
	}

	psqlConn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", cfg.Host, cfg.DBPort, cfg.User, cfg.Password, cfg.DBname)
	pool, err := pgxpool.Connect(ctx, psqlConn)
	if err != nil {
		log.Fatal("can't connect to database", err)
	}
	defer pool.Close()

	if err := pool.Ping(ctx); err != nil {
		log.Fatal("ping database error", err)
	}

	config := pool.Config()
	config.MaxConnIdleTime = cfg.MaxConnIdleTime
	config.MaxConnLifetime = cfg.MaxConnLifetime
	config.MinConns = cfg.MinConns
	config.MaxConns = cfg.MaxConns
	boxStorage := pgboxstorage.New(pool)
	packStorage := pgpackstorage.New(pool)
	productStorage := pgproductstorage.New(pool)

	
	packApp := packapp.New(packStorage)
	boxApp := boxapp.New(boxStorage, *packApp)
	productApp := productapp.New(productStorage)

	initproducts.InitProductsTable(cfg.CsvInitFile, *productApp)

	runREST(*cfg, *boxApp, *packApp, *productApp)
}
