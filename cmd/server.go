package main

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Hostile359/expero_test/internal/api"
	"gitlab.com/Hostile359/expero_test/internal/app/boxapp"
	"gitlab.com/Hostile359/expero_test/internal/app/packapp"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
)

func runREST(cfg Config, boxApp boxapp.App, packApp packapp.App, productApp productapp.App) {
	apiServer := api.New(boxApp, packApp, productApp)
	http.HandleFunc("/aggr", apiServer.Aggregation)
	http.HandleFunc("/get_boxes", apiServer.GetBoxesBySgtins)
	http.HandleFunc("/get_packs", apiServer.GetPacksByGtin)
	http.Handle("/metrics", promhttp.Handler())

	if err := http.ListenAndServe(cfg.HttpPort, nil); err != nil {
		log.Fatalln(err)
	}
}