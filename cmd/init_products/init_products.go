package initproducts

import (
	"encoding/csv"
	"io"
	"os"
	"strconv"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"gitlab.com/Hostile359/expero_test/internal/app/productapp"
	"gitlab.com/Hostile359/expero_test/internal/entities/product"
)

func InitProductsTable(pathToCsvInitFile string, productApp productapp.App) {
	file, err := os.Open(pathToCsvInitFile)
	if err != nil {
		log.Fatal("Error while init products", err)
	}
	defer file.Close()

	r := csv.NewReader(file)
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal("Error while init products", err)
		}
		if len(record) != 3 {
			log.Fatalf("Wrong records in %s", pathToCsvInitFile)
		}
		name := record[0]
		gtin := record[1]
		packs, err := strconv.Atoi(record[2])
		if err != nil {
			log.Fatal("Error while init products", err)
		}

		p := product.NewProduct(name, gtin, packs)
		
		if err = productApp.Add(p); err != nil {
			if !errors.Is(err, productapp.ErrProductExists) {
				log.Fatal("Error while init products", err)
			}
		}
	}
}
