# syntax=docker/dockerfile:1

FROM golang:1.18.4-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY cmd ./cmd
COPY internal ./internal
COPY resources ./resources
RUN go build -o ./main cmd/main.go cmd/server.go cmd/config.go

EXPOSE 8080

CMD [ "./main" ]