#!/bin/sh

export DB_DSN="host=${HOST} port=${DB_PORT} user=${DB_USER} password=${DB_PASSWORD} dbname=${DB_NAME} sslmode=disable"

goose postgres "${DB_DSN}" up