.PHONY: run, build
run:
	go run cmd/main.go cmd/server.go cmd/config.go --local

build:
	go build -o bin/main cmd/main.go cmd/server.go cmd/config.go

clean:
	rm -rf bin

LOCAL_BIN:=$(CURDIR)/bin
.PHONY: .deps
.deps:
	GOBIN=$(LOCAL_BIN) go install github.com/pressly/goose/v3/cmd/goose@latest

.PHONY: up-db
up-db:
	docker-compose build
	docker-compose up -d postgres

up-docker:
	docker-compose build
	docker-compose up -d

down-docker:
	docker-compose down

MIGRATIONS_DIR:=./migrations
.PHONY: migration
migration:
	bin/goose -dir=${MIGRATIONS_DIR} create $(NAME) sql 

.PHONY: .test
.test:
	$(info Running tests...)
	go test -v $$(go list ./... | grep -v -E '/homework-1/pkg/(api)')

.PHONY: .integration_test
.integration_test:
	$(info Running tests...)
	go test -v -tags integration ./tests/...

.PHONY: cover
cover:
	go test -v $$(go list ./... | grep -v -E '/homework-1/pkg/(api)') -covermode=count -coverprofile=/tmp/c.out
	go tool cover -html=/tmp/c.out
